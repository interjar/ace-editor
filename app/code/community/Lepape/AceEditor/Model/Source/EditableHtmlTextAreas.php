<?php

class Lepape_AceEditor_Model_Source_EditableHtmlTextAreas
{
    public function toOptionArray()
    {
        $helper = Mage::helper('ace_editor');
        return array(
            array('value' => '#description', 'label' => $helper->__('Product Description')),
            array('value' => '#short_description', 'label' => $helper->__('Product Short Description')),
            array('value' => '#page_content', 'label' => $helper->__('CMS Page Content')),
            array('value' => '#block_content', 'label' => $helper->__('Static Block Content')),
            array('value' => '#_generaldescription', 'label' => $helper->__('Category Description')),
            array('value' => '#group_3description', 'label' => $helper->__('Category Description #2')),
            array('value' => '#text', 'label' => $helper->__('Newsletter Template')),
            array('value' => '#template_text', 'label' => $helper->__('Email Templates')),
        );
    }
}

